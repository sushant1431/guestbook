# Guest Book

## Description
Guest Project demonstrate following features

* Spring Security 
* Bootstrap Integration
* Spring Data
* Spring Boot Features
* Java 8
### Guides
To run this application. application.yml file need to have correct MySQL detail
    driverClassName: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://172.17.0.2:3306/guestbook
    username: root
    password: root
    
1) After updating run maven package from terminal in the location where pom.xml resides
2) With above action it will create guestbook-0.0.1-SNAPSHOT.jar in target directory
3) To run this jar enter command java -jar guestbook-0.0.1-SNAPSHOT.jar
4) Now browser http://localhost:8018/login
5) For this application I have created two user with two roles that is 'user' and 'admin'.
6) To login as user, enter credential user / password, this will take you to index page initally it will have zero feedbacks
    to enter feedback click on Post Feedback, fill the form and click post.
7) After this it will take you to feedback list. user will be able to see it's post until admin approves post.
8) To login as admin user, login using credential admin / admin
9) After login it will land you to feedback list page. where admin can approve post. After approval normal user could see the post.


