package com.bt.guestbook.controller;

import com.bt.guestbook.models.FeedBack;
import com.bt.guestbook.services.GuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class GuestController {

    @Autowired
    GuestService service;

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping({"/index"})
    public String index(Model model) {
        String role =  SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().findFirst().get().getAuthority();
        List<FeedBack> feedBacks =service.getFeedbacks();
        if(!role.equals("ROLE_ADMIN"))
        feedBacks = feedBacks.stream().filter(feedBack -> feedBack.isApproved()).collect(Collectors.toList());
        model.addAttribute("feedbacks", feedBacks);
        return "index";
    }

    @GetMapping({"/commentForm"})
    public String commentForm(Model model) {
        FeedBack feedBack = new FeedBack();
        model.addAttribute("feedback", feedBack);
        return "commentForm";
    }

    @PostMapping({"/commentForm"})
    public String commentFormSubmit( Model model, @ModelAttribute FeedBack feedBack) {
        service.save(feedBack);
        model.addAttribute("feedbacks", service.getFeedbacks());
        return "index";
    }

    // Login form with error
    @RequestMapping("/loginError")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "error";
    }

    @GetMapping("/approve")
    public String approve(@RequestParam(value="id") String id){
        int iid = Integer.parseInt(id);
        service.updateFeedback(iid);
        return "redirect:/index";
    }



}
