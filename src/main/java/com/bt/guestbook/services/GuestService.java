package com.bt.guestbook.services;

import com.bt.guestbook.models.FeedBack;
import com.bt.guestbook.repository.FeedBackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GuestService {
    @Autowired
    FeedBackRepository feedBackRepository;

    public String save(FeedBack feedBack) {
        FeedBack f = feedBackRepository.save(feedBack);
        return "saved";
    }

    public List<FeedBack> getFeedbacks() {
        List<FeedBack> feedBacks = feedBackRepository.findAll();
        return feedBacks;
    }

    public void updateFeedback(int id) {
        Optional<FeedBack> optionalFeedBack = feedBackRepository.findById(id);
        if(optionalFeedBack.isPresent()){
            FeedBack feedBack = optionalFeedBack.get();
            feedBack.setApproved(true);
            feedBackRepository.save(feedBack);
        }
    }
}
